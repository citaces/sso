package main

import (
	"log/slog"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/citaces/sso/internal/app"
	"gitlab.com/citaces/sso/internal/config"
)

const (
	envLocal = "local"
	envDev   = "dev"
	envProd  = "prod"
)

func main() {
	// ициализируем объект конфига

	cfg := config.MustLoad()

	// инициализируем логгер

	log := setupLogger(cfg.Env)

	// инициализируем приложение

	application := app.New(log, cfg.GRPC.Port, cfg.StoragePath, cfg.TokenTTL)

	// запустим grpc сервер приложения

	go application.GRPCSrv.MustRun()

	// graceful shutdown

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)
	sign := <-signalChan
	log.Info("stopping app...", slog.String("signal:", sign.String()))
	application.GRPCSrv.Stop()
	log.Info("app is gracefully stopped by the signal", slog.String("signal:", sign.String()))
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case envLocal:
		log = slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case envDev:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))
	case envProd:
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}))
	}
	return log
}
