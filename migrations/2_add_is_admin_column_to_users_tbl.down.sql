CREATE TEMPORARY TABLE IF NOT EXISTS users_backup
(
    id INTEGER PRIMARY KEY,
    email TEXT NOT NULL UNIQUE,
    pass_hash BLOB NOT NULL
);

INSERT INTO users_backup SELECT id, email, pass_hash FROM users;

DROP TABLE users;

CREATE TABLE users
(
    id INTEGER PRIMARY KEY,
    email TEXT NOT NULL UNIQUE,
    pass_hash BLOB NOT NULL
);
CREATE INDEX IF NOT EXISTS idx_email ON users (email);

INSERT INTO users SELECT id, email, pass_hash FROM users_backup;

DROP TABLE users_backup;
