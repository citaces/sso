package config

import (
	"flag"
	"os"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Env         string        `yaml:"env" env-default:"local"`
	StoragePath string        `yaml:"storage_path" env-required:"true"`
	GRPC        GRPCConfig    `yaml:"grpc" env-required:"true"`
	TokenTTL    time.Duration `yaml:"token_ttl"`
}

type GRPCConfig struct {
	Port    int           `yaml:"port"`
	Timeout time.Duration `yaml:"timeout"`
}

func MustLoad() *Config {
	path := fetchConfigPath()
	if len(path) == 0 {
		panic("empty config path")
	}
	return MustLoadByPath(path)
}

// fetchConfigPath - получает инфу от пути до файла конфига из одного из двух источников, либо
// из флага или переменной окружения
func fetchConfigPath() string {
	var res string

	flag.StringVar(&res, "config", "", "path to config file")
	flag.Parse()

	if len(res) == 0 {
		res = os.Getenv("CONFIG_PATH")
	}
	return res
}

func MustLoadByPath(path string) *Config {
	// проверим, что по этому пути что-то есть
	if _, err := os.Stat(path); os.IsNotExist(err) {
		panic("config path not exists")
	}
	var cfg Config
	if err := cleanenv.ReadConfig(path, &cfg); err != nil {
		panic("failed to read config:" + err.Error())
	}
	return &cfg
}
