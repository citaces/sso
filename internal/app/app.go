package app

import (
	"log/slog"
	"time"

	grpcApp "gitlab.com/citaces/sso/internal/app/grpc"
	"gitlab.com/citaces/sso/internal/services/auth"
	"gitlab.com/citaces/sso/internal/services/storage/sqlite"
)

type App struct {
	GRPCSrv *grpcApp.App
}

func New(log *slog.Logger, grpcPort int, storagePath string, tokenTTL time.Duration) *App {
	// TODO: инициализировать хранилище

	// TODO: init auth service (auth)
	storage, err := sqlite.New(storagePath)
	if err != nil {
		panic(err)
	}

	authService := auth.New(log, storage, storage, storage, tokenTTL)
	grpcApplication := grpcApp.New(log, authService, grpcPort)
	return &App{GRPCSrv: grpcApplication}
}
