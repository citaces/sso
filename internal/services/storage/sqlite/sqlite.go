package sqlite

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/mattn/go-sqlite3"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/citaces/sso/internal/domain/models"
	"gitlab.com/citaces/sso/internal/services/storage"
)

type Storage struct {
	db *sql.DB
}

func (s *Storage) User(ctx context.Context, email string) (models.User, error) {
	op := "storage.User"
	stmt, err := s.db.Prepare("SELECT id,email,pass_hash FROM users WHERE email = ?")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}
	defer stmt.Close()
	row := stmt.QueryRowContext(ctx, email)
	var target models.User
	if err := row.Scan(&target.ID, &target.Email, &target.PassHash); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, storage.ErrUserNotFound
		}
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}
	return target, nil
}

func (s *Storage) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	op := "storage.IsAdmin"
	stmt, err := s.db.Prepare("SELECT is_admin FROM users WHERE id = ?")
	defer stmt.Close()
	if err != nil {
		return false, fmt.Errorf("%s: %w", op, err)
	}
	row := stmt.QueryRowContext(ctx, userID)
	var res bool
	if err := row.Scan(&res); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, storage.ErrAppNotFound
		}
		return false, fmt.Errorf("%s: %w", op, err)
	}
	return res, nil
}

func (s *Storage) SaveUser(ctx context.Context, email string, passHash []byte) (uid int64, err error) {
	op := "storage.SaveUser"
	stmt, err := s.db.Prepare("INSERT INTO users(email,pass_hash) VALUES(?,?)")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	defer stmt.Close()
	res, err := stmt.ExecContext(ctx, email, passHash)
	if err != nil {
		var sqliteErr sqlite3.Error
		if errors.As(err, &sqliteErr) && errors.Is(sqliteErr.ExtendedCode, sqlite3.ErrConstraintUnique) {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrUserExists)
		}
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	return id, nil
}

func New(storagePath string) (*Storage, error) {
	op := "sqlite.New"
	db, err := sql.Open("sqlite3", storagePath)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}
	return &Storage{db: db}, nil
}

func (s *Storage) App(ctx context.Context, appID int) (models.App, error) {
	op := "storage.App"
	stmt, err := s.db.Prepare("SELECT id, name, secret FROM apps WHERE id = ?")
	if err != nil {
		return models.App{}, fmt.Errorf("%s: %w", op, err)
	}
	defer stmt.Close()
	row := stmt.QueryRowContext(ctx, appID)
	var res models.App
	if err := row.Scan(&res.ID, &res.Name, &res.Secret); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.App{}, fmt.Errorf("%s: %w", op, storage.ErrAppNotFound)
		}
		return models.App{}, fmt.Errorf("%s: %w", op, err)
	}
	return res, nil
}
