package auth

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"strconv"
	"time"

	"gitlab.com/citaces/sso/internal/domain/models"
	"gitlab.com/citaces/sso/internal/lib/jwt"
	"gitlab.com/citaces/sso/internal/services/storage"
	"golang.org/x/crypto/bcrypt"
)

var (
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrInvalidAppID       = errors.New("invalid app id")
	ErrUserExists         = errors.New("user exists")
)

type UserSaver interface {
	SaveUser(ctx context.Context, email string, passHash []byte) (uid int64, err error)
}

type UserProvider interface {
	User(ctx context.Context, email string) (models.User, error)
	IsAdmin(ctx context.Context, userID int64) (bool, error)
}

type AppProvider interface {
	App(ctx context.Context, appID int) (models.App, error)
}

type Auth struct {
	log          *slog.Logger
	userSaver    UserSaver
	userProvider UserProvider
	appProvider  AppProvider
	tokenTTL     time.Duration
}

func (a *Auth) Login(ctx context.Context, email string, password string, appID int) (string, error) {
	op := "auth.Login"

	log := a.log.With(slog.String("op", op), slog.String("username", email))
	log.Info("attempting to login user")

	user, err := a.userProvider.User(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			log.Warn("user not found", err)
			return "", fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
		}
		log.Error("failed to get user", err)
		return "", fmt.Errorf("%s: %w", op, err)
	}
	if err := bcrypt.CompareHashAndPassword(user.PassHash, []byte(password)); err != nil {
		log.Error("wrong password", err)
		return "", fmt.Errorf("%s: %w", op, ErrInvalidCredentials)
	}
	app, err := a.appProvider.App(ctx, appID)
	if err != nil {
		if errors.Is(err, storage.ErrAppNotFound) {
			log.Warn("app not found", err)
			return "", fmt.Errorf("%s: %w", op, ErrInvalidAppID)
		}
		log.Error("cant app login", storage.ErrAppNotFound)
		return "", fmt.Errorf("%s: %w", op, storage.ErrAppNotFound)
	}
	log.Info("successfully login")

	token, err := jwt.NewToken(user, app, a.tokenTTL)
	if err != nil {
		log.Error("token error", err)
		return "", fmt.Errorf("%s: %w", op, err)
	}

	return token, nil

}

func (a *Auth) RegisterNewUser(ctx context.Context, email string, password string) (UserID int64, err error) {
	op := "auth.RegisterNewUser"

	log := a.log.With(
		slog.String("op", op),
		slog.String("email", email))
	log.Info("registering user")

	passHash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate password hash", err)
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	id, err := a.userSaver.SaveUser(ctx, email, passHash)
	if err != nil {
		if errors.Is(err, storage.ErrUserExists) {
			log.Warn("user exists", err)
			return 0, fmt.Errorf("%s: %w", op, ErrUserExists)
		}
		log.Error("failed to save user", err)
		return 0, fmt.Errorf("%s: %w", op, err)
	}
	log.Info("user successful registered with user_id", id)
	return id, nil
}

func (a *Auth) IsAdmin(ctx context.Context, userID int64) (bool, error) {
	op := "auth.IsAdmin"

	log := a.log.With(slog.String("op", op), slog.String("user_id", strconv.Itoa(int(userID))))
	log.Info("checking if user is admin")
	isAdmin, err := a.userProvider.IsAdmin(ctx, userID)
	if err != nil {
		if errors.Is(err, storage.ErrAppNotFound) {
			log.Warn("app is not found", err)
			return false, fmt.Errorf("%s: %w", op, ErrInvalidAppID)
		}
		log.Error("cant get admin status", err)
		return false, fmt.Errorf("%s: %w", op, err)
	}
	log.Info("successfully checked if user is admin", slog.Bool("is_admin", isAdmin))
	return isAdmin, nil
}

func New(log *slog.Logger, userSaver UserSaver, userProvider UserProvider, appProvider AppProvider, tokenTTL time.Duration) *Auth {
	return &Auth{log: log, userSaver: userSaver, userProvider: userProvider, appProvider: appProvider, tokenTTL: tokenTTL}
}
