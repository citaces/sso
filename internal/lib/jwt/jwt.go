package jwt

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/citaces/sso/internal/domain/models"
)

func NewToken(user models.User, app models.App, duration time.Duration) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		claims["uid"] = user.ID
		claims["email"] = user.Email
		claims["exp"] = time.Now().Add(duration).Unix()
		claims["app_id"] = app.ID

		tokenString, err := token.SignedString([]byte(app.Secret))
		if err != nil {
			return "", err
		}

		return tokenString, nil
	}
	return "", errors.New("cant generate token")
}
